const { User, validateUser, validateUserForUpdate } = require("../../models/user/user");
const bcrypt = require("bcrypt");
const _ = require("lodash");
const Joi = require("joi");
const { getQueryParameters } = require("../../config/helper/utils");

exports.signup = async (req, res) => {
  const { error } = validateUser(req.body);
  if (error) return res.status(400).send(error.details.map(e => e.message));

  let user = await User.findOne({ email: req.body.email });
  if (user)
    return res
      .status(400)
      .send("User already registered with this email account.");

      user = req.body;
  user.totalSaleCount = 0;
  user.totalSale = 0;
  user.totalBuyCount = 0;
  user.totalBuy = 0;
  user = new User(req.body);
  console.log(user);

  const salt = bcrypt.genSaltSync(10);
  user.password = bcrypt.hashSync(user.password, salt);

  await user.save();

  const token = user.generateAuthToken();
  res
    .status(200)
    .send({token, user});
};

exports.getProfile = async (req, res) => {
  const user = await User.findById(req.user._id);
  return res.send(user);
};

exports.updatePassword = async(req, res)=>{

  const{error} = validateUpdatePassword(req.body);
  if(error) return res.status(400).send(error.details.map(e => e.message));

  const salt = bcrypt.genSaltSync(10);
  console.log(req.params.userId)
  await User.findByIdAndUpdate(req.params.userId, {
    $set: {password: bcrypt.hashSync(req.body.newPassword, salt)}
  });

  // let result = await user.save();
  return res.status(200).send('password changed successfully !');
  
}


const validateUpdatePassword = (data)=>{
  const schema = {
    newPassword: Joi.string().min(6).required()
  }
  return Joi.validate(data,schema);
}

exports.addUser = async (req, res) => {
  const { error } = validateUser(req.body);
  if (error) return res.status(422).send(error.details.map(e => e.message));
  try {
    user = await User.findOne({ email: req.body.email });
    if (user)
      return res
        .status(409)
        .send("User already registered with this email account.");

    // user = new User(_.pick(req.body, ["username", "password", "email"]));
    user = req.body;
    user.totalSaleCount = 0;
    user.totalSale = 0;
    user.totalBuyCount = 0;
    user.totalBuy = 0;

    user = new User(user);

    const salt = await bcrypt.genSalt(10);
    const password = await bcrypt.hash(user.password, salt);
    user.password = password;
    user = await user.save();
    const token = user.generateAuthToken();

    // delete req.body.password;
    // let customer = new Customer(req.body);
    // customer._id = user._id;

    // customer = await customer.save();
    return res
      .header("x-auth-token", token)
      .status(200)
      .send(user);
  } catch (e) {
    console.log(e);
  }
};

exports.getAllUsers = async (req, res) => {
  try {
    const users = await User.find(getQueryParameters(req.query));
    return res.status(200).send(users);
  } catch (error) {
    res.status(404).send(error);
  }
};


exports.deleteUser = async (req, res) => {
  const user = await User.findById(req.params.userId).select(
    "-password"
  );
  let result = null;
  if (user) {
    result = await user.remove();
  }
  return res.send(result);
};

exports.updateUser = async (req, res) => {
  const { error } = validateUserForUpdate(req.body);
  if (error) return res.status(400).send(error.details.map(e => e.message));
  const userId = req.params.userId;

  req.body.updatedAt = new Date();

  // req.body.updatedAt = new Date();
  await User.findByIdAndUpdate(userId, {$set: req.body}, { useFindAndModify: false });
  const updatedUser = await User.findById(userId);
  return res.status(200).send(updatedUser);
};

exports.getUserById = async (req, res) => {
  const user = await User.findById(req.params.id).select('-password -__v');
  return res.send(user);
};

exports.getSelf = async (req, res) => {
  console.log(req.user);
  let user = await User.findById(req.user._id);
  return res.status(200).send(user);
};