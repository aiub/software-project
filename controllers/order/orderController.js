const _ = require("lodash");
const { User } = require("../../models/user/user");
const { Order } = require("../../models/order/order");

const { ProductVariant } = require("../../models/product/productVariant");
const { Product } = require("../../models/product/product");
const { response } = require("express");

//post a order
exports.addOrder = async (req, res) => {
  let {itemList, deliveryAddress, orderNote} = req.body;
  
  //update variant quantity in database.

  let totalDiscount = 0;
  let totalCost = 0;
  let actualCost = 0;

  for(let i=0; i<itemList.length; i++){
    let item = itemList[i];

    let product = await Product.findById(item.productId).populate('productVariant');
    let productVariant = product.productVariant;
    
    for(let j=0; j< productVariant.variants.length; j++){
      let variant = productVariant.variants[j];
      if(variant.attrs.colorFamily === item.color && variant.attrs.size === item.size){
        variant.quantity = variant.quantity - item.quantity;
        item.image = variant.images[0];
      }
    }
    await ProductVariant(productVariant).save();
    product.totalSaleCount = product.totalSaleCount + item.quantity;
    await Product(product).save();



    //calculate prices.
    let discount = ((product.price * product.discountPercentage) / 100 ) * item.quantity;
    let price = product.price * item.quantity;
    totalDiscount = totalDiscount + discount;
    actualCost = actualCost + price;
    totalCost = totalCost + (price - discount)

    //calculate seller info
    let seller = await User.findById(product.seller);
    if(seller){
      seller.totalSaleCount = seller.totalSaleCount + 1;
      seller.totalSale = seller.totalSale + (price - discount);
      await seller.save();
    }
  }

  let order = new Order({
    customer: req.user._id,
    deliveryAddress,
    orderNote,
    orderedProducts: itemList,
    totalCost,
    totalDiscount,
    actualCost,
    status: 'Order has been placed'
  });
  await order.save();

  let customer = await User.findById(req.user._id);
  customer.totalBuyCount = customer.totalBuyCount + 1;
  customer.totalBuy = customer.totalBuy + totalCost;

  await User(customer).save();

  res.status(200).send(req.body);
};

//GET ALL order
exports.getAllOrder = async (req, res) => {
  try {

    if(req.user.userType == 'CUSTOMER'){
      const order = await Order.find({customer: req.user._id}).sort({createdAt: -1});
      return res.status(200).send(order);
    }else{
      const order = await Order.find().sort({createdAt: -1});
      return res.status(200).send(order);
    }
    
  } catch (err) {
    return response.status(500).send('internal server error');
  }
};

//get a single order By id
exports.getOrderById = async (req, res) => {
  try {
    const order = await Order.findById(req.params.orderId).populate({path: 'customer', select: '-password -__v -active'});
    if(!order){
      return res.status(404).send('No order found by id ' + req.params.id);
    }
    return res.send(order);
  } catch (err) {
    console.log(err);
    return res.status(500).send('internal server error');
  }
};


exports.changeOrderStatus = async (req, res) => {
  try{
    let  id = req.params.orderId;
    let order = await Order.findByIdAndUpdate(id, {$set: {status: req.body.status}});

    return res.status(200).send(order);

  }catch(error){
    return res.status(500).send('internal server error');
  }
}

