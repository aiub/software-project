const bcrypt = require("bcrypt");
const _ = require("lodash");
const Joi = require("joi");
const { User } = require("../../models/user/user");

exports.login = async (req, res) => {
  // const { error } = validate(req.body);
  // if (error) return res.status(400).send(error.details[0].message);

  try{
    let user = await User.findOne({ email: req.body.email });
    console.log('here')
    if (!user) return res.status(400).send("Invalid email or password.");
    
    const validPassword = bcrypt.compareSync(req.body.password, user.password);
    if (!validPassword) return res.status(400).send("Invalid email or password.");
  
    
    const token = user.generateAuthToken();
    return res.send({user,token});
  }catch(error){
    console.log(error);
    return res.status(500).send('Internal Server Error !');
  }

};

function validate(req) {
  const schema = {
    email: Joi.string()
      .email()
      .required(),
    password: Joi.string()
      .min(6)
      .max(255)
      .required()
  };

  return Joi.validate(req, schema);
}
