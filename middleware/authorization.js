exports.admin = (req, res, next) => {
  // 401 Unauthorized
  // 403 Forbidden

  if (!req.user.userType === 'SYSTEM_ADMIN') return res.status(403).send("Access denied.");

  next();
};

exports.staff = (req, res, next) => {
  if (req.user.userType === 'SYSTEM_AGENT' || req.user.userType === 'SYSTEM_ADMIN')
    return next();

  return res.status(403).send("Access denied.");
};

exports.superUser = (req, res, next) => {
  if (!req.user.userType === 'SYSTEM_ADMIN') return res.status(403).send("Access denied.");

  next();
};

exports.self = (req, res, next) => {
  if (req.user._id === req.params.userId || req.user._id === req.body._id)
    next();
  else return res.status(403).send("Access denied. Inappropiet user.");
};

exports.selfOrStaff = (req, res, next) => {
  if (
    req.user._id === req.params.userId ||
    req.user.userType == 'SYSTEM_ADMIN' ||
    req.user.userType == 'SYSTEM_AGENT'
  )
    next();
  else return res.status(403).send("Access denied. Inappropiet user.");
};
