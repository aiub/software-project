const express = require("express");
const {
  addProduct,
  getProducts,
  getProductAdditionalInfo,
  getProductById,
  getProductByTopDiscount,
  getProductByBrandName,
  getProductByDeptName,
  updateProduct,
  deleteProduct,
  getHotDepartmentsProducts,
  updateShipping,
  getProductsPublic,
  approveProduct,
  makeProductNew
} = require("../../controllers/product/productController");
const router = express.Router();

const { staff } = require("../../middleware/authorization");
const auth = require("../../middleware/auth");

//Post Product
router.post("/post",[auth], addProduct);
router.put("/approve/:id",[auth, staff], approveProduct);
router.put("/make-new/:id",[auth, staff], makeProductNew);

//Get All Products
router.get("/",[auth], getProducts);

router.get("/public", getProductsPublic);

//Get product additional info.
router.get("/additional-info/:id", getProductAdditionalInfo);

//Get Product by Id
router.get("/id/:id", getProductById);



//Get Product by department name
router.get("/department/:name", getProductByDeptName);

//Get Product by Brand Name
router.get("/brand/:name", getProductByBrandName);

//Get Product by Top Discount
router.get("/top-discounts", getProductByTopDiscount);

//Put Request
//router.put("/:id", [auth, staff], updateProduct);

//Put Request
router.put("/update/:id",[auth, ], updateProduct);
router.put("/shipping/update/:id",[auth], updateShipping);

//Delete Request
router.delete("/delete/:id", deleteProduct);

//hot department
router.get("/hot-departments", getHotDepartmentsProducts);

module.exports = router;
