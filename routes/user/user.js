const auth = require("../../middleware/auth");
const {staff,selfOrStaff,admin} = require("../../middleware/authorization");
const {
    signup,
    getProfile,
    addUser,
    getAllUsers,
getUserById,
deleteUser,
updateUser,
updatePassword,
getSelf} = require("../../controllers/user/userController");
const express = require("express");
const router = express.Router();

router.get("/profile", auth, getProfile);

router.post("/signup", signup);

router.put("/change-password/:userId", [auth,selfOrStaff], updatePassword);

router.post("/post", addUser);

// router.get("/", [auth, staff], getAllUsers);
router.get("/", getAllUsers);

router.get("/id/:id", getUserById); //[auth, selfOrStaff]

router.get("/self", [auth], getSelf);

router.delete("/delete/:id", [auth, staff], deleteUser);

router.put("/update/:userId", [auth, selfOrStaff], updateUser);

module.exports = router;
