
const express = require("express");
const router = express.Router();

// const app = express();

const fs = require("fs");
const path = require("path");
const app = express();

const multer = require("multer");

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    let date = new Date();
    date = "" + date.getDate() + date.getMonth() + date.getFullYear();
    const uploadDir = path.join(
      __dirname,
      "..",
      "..",
      "public",
      "uploads",
      "products",
      `${date}`
    );
    fs.mkdir(uploadDir, function (e) {
      if (!e || (e && e.code === "EEXIST")) {
        cb(null, uploadDir);
      } else {
        console.log("File creation failed,", e);
      }
    });
  },
  filename: (req, file, cb) => {
    cb(
      null,
      file.fieldname + "-" + new Date().toISOString().replace(':', '-') + path.extname(file.originalname)
    );
  }
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

var upload = multer({ storage: fileStorage, fileFilter: fileFilter }).fields([
  {
    name: "file",
    maxCount: 1
  },
  {
    name: "images",
    maxCount: 100
  }
]);

router.post("/product", upload, (req, res) => {
    if(!req.files){
        console.log('no files');
        return res.status(422).send('No file uploaded.');
    }
    const path = req.files.file[0].path.split('/software-project/')[1];
    return res.status(200).send("/"+path);
});

module.exports = router;