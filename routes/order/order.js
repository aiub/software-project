
const express = require("express");
const router = express.Router();

const { addOrder, getAllOrder, getOrderById, changeOrderStatus } = require("../../controllers/order/orderController");
const auth = require("../../middleware/auth");
const {staff} = require('../../middleware/authorization');

router.post("/post",[auth], addOrder);
router.get("/",[auth], getAllOrder);
router.get("/id/:orderId",[auth], getOrderById);
router.put("/change-status/:orderId", [auth, staff], changeOrderStatus);

module.exports = router;