
const express = require("express");
const auth = require("../middleware/auth");
const { User } = require("../models/user/user");
const { Order } = require("../models/order/order");

const router = express.Router();

router.get("",[auth], async (req, res) => {
    const users = await User.find({userType: 'CUSTOMER'});

    let dates = users.map(e => e.createdAt.toLocaleDateString());
    let uniqueDates = [...new Set([...dates])];

    let customerGraph = [];
    for(let i=0; i<uniqueDates.length; i++){
        let totalReg =0;
        for(let j=0; j<users.length; j++){
            if(users[j].createdAt.toLocaleDateString() == uniqueDates[i]){
                totalReg = totalReg + 1;
            }
        }
        customerGraph.push({
            label: 'Registration',
            count: totalReg,
            date: uniqueDates[i]
        })
    }

    const orders = await Order.find({status: 'Order has been delivered'});
    dates = orders.map(e => e.createdAt.toLocaleDateString());
    uniqueDates = [...new Set([...dates])];

    let saleGraph = [];
    for(let i=0; i<uniqueDates.length; i++){
        let totalSale =0;
        for(let j=0; j<orders.length; j++){
            if(orders[j].createdAt.toLocaleDateString() == uniqueDates[i]){
                totalSale = totalSale + orders[j].totalCost;
            }
        }
        saleGraph.push({
            label: 'Sale',
            totalPrice: totalSale,
            date: uniqueDates[i]
        })
    }

    res.status(200).send({
        customerGraph,
        saleGraph,
        customerCount: users.length,
        saleCount: orders.length 
    });

});

module.exports = router;
