
exports.getQueryParameters = (query) => {
    if(!query) return {};
    const queryParams = {};

    for(let e in query){
      if(query[e]){
        queryParams[e] = isNaN(query[e]) && query[e] != 'false' && query[e] != 'true' ?
         {$regex: query[e], $options: 'i'} :
         query[e];
      }
    }

    return queryParams;
}