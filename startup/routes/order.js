//order request
const order = require("../../routes/order/order");
module.exports = function (app) {
  app.use("/api/v1/orders", order);
};
