const brand = require("../../routes/product/brand");
const product = require("../../routes/product/product");
const productVariant = require("../../routes/product/productVariant");
const express = require("express");

// const app = express();

const fs = require("fs");
const path = require("path");
const app = express();

const multer = require("multer");

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    let date = new Date();
    date = "" + date.getDate() + date.getMonth() + date.getFullYear();
    const uploadDir = path.join(
      __dirname,
      "..",
      "..",
      "public",
      "uploads",
      "products",
      `${date}`
    );
    fs.mkdir(uploadDir, function (e) {
      if (!e || (e && e.code === "EEXIST")) {
        cb(null, uploadDir);
      } else {
        console.log("File creation failed,", e);
      }
    });
  },
  filename: (req, file, cb) => {
    cb(
      null,
      file.fieldname + "-" + new Date().toISOString().replace(':', '-') + path.extname(file.originalname)
    );
  }
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

var upload = multer({ storage: fileStorage, fileFilter: fileFilter }).fields([
  {
    name: "thumbnailImages",
    maxCount: 1
  },
  {
    name: "images",
    maxCount: 100
  }
]);

module.exports = function (app) {
  app.use("/api/v1/brand", brand);
  app.use("/api/v1/products", upload, product);
  app.use("/api/v1/product-variant", upload, productVariant);
};
