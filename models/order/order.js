const mongoose = require("mongoose");
const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);

const orderedProductSchema = new mongoose.Schema({
  productId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Product"
  },
  color: {
    type: String,
  }, 
  size: {
    type: String,
  },
  image: {
    type: String,
  },
  quantity: {
    type: Number,
    required: "quantity is required"
  }
});

const orderSchema = new mongoose.Schema({
  customer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: "customer id is required"
  },
  orderedProducts: [
    {
      type: orderedProductSchema,
      required: true
    }
  ],
  actualCost: {
    type: Number
  },
  totalDiscount: {
    type: Number
  },
  totalCost: {
    type: Number
    // required:"total cost required"
  },
  deliveryAddress: {
    type: String,
    required: "customer id is required"
  },
  orderNote: {
    type: String,
  },
  createdAt: {
    type: Date,
  },
  updatedAt: {
    type: Date,
  },
  status: {
    type: String
  }
}, {
  timestamps: true
});

const Order = mongoose.model("Order", orderSchema);

function validateOrder(order) {
  const schema = {
    customer: Joi.objectId().required(),
    orderedProducts: Joi.array()
      .items({
        productId: Joi.objectId().required(),

        quantity: Joi.number().required()
      })
      .required(),
    actualCost: Joi.number(),
    totalDiscount: Joi.number(),
    totalCost: Joi.number(),
    deliveryAddress: Joi.string(),
    orderNote: Joi.string(),
    createdAt: Joi.date(),
    updatedAt: Joi.date(),
    status: Joi.string()
  };
  return Joi.validate(order, schema, {
    abortEarly: false
  });
}

module.exports = {
  validateOrder,
  Order
};
