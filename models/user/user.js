const config = require("config");
const validator = require("validator");
const jwt = require("jsonwebtoken");
const Joi = require("joi");
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

  email: {
    type: String,
    lowercase: true,
    trim: true,
    unique: true,
    required: "Email address is required",
    validate: {
      validator: validator.isEmail,
      message: "Please enter a valid email address"
    },
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      "Please fill a valid email address"
    ]
  },
  //phone field will be changed with proper validation
  password: {
    type: String,
    maxlength: 255,
    minlength: 6
  },
  phone: {
    type: String
  },
  gender: {
    type: String
  },
  fullName: {
    type: String,
    maxlength: 50,
    minlength: 3,
    required: true,
    trim: true
  },
  
  totalBuy: {
    type: Number,
    min: 0
  },

  totalBuyCount: {
    type: Number,
    min: 0
  },

  totalSale: {
    type: Number,
    min: 0
  },

  totalSaleCount: {
    type: Number,
    min: 0
  },

  deliveryAddress: {
    type: String,
  },
  userType: {
    type: String,
  },
  createdAt: {
    type: Date
  },
  updatedAt: {
    type: Date
  },

  active: {
    type:Boolean,
    default: true
  },
},
{
  timestamps: true
});

userSchema.methods.generateAuthToken = function () {
  const token = jwt.sign(
    {
      _id: this._id,
      userType: this.userType,
      email: this.email,
      fullName: this.fullName
    },
    config.get("jwtPrivateKey")
  );
  return token;
};

const User = mongoose.model("User", userSchema);

// var validateEmail = function(email) {
//   var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
//   return re.test(email);
// };

function validateUser(user) {
  const schema = {
    password: Joi.string()
      .min(6)
      .max(30)
      .required(),
    email: Joi.string()
      .email()
      .required(),
    phone: Joi.string(),
    gender: Joi.string(),
    fullName: Joi.string()
      .max(50)
      .min(3)
      .required(),
    
    deliveryAddress: Joi.string(),
    userType: Joi.string(),
    active: Joi.boolean(),
    isSuperUser: Joi.boolean(),
    isAdmin: Joi.boolean(),
    isStaff: Joi.boolean(),
    createdAt: Joi.date(),
    updatedAt: Joi.date()
  };

  return Joi.validate(user, schema);
}

function validateUserForUpdate(user) {
  const schema = {
    email: Joi.string()
      .email(),
    phone: Joi.string(),
    fullName: Joi.string()
      .max(50)
      .min(3)
      .required(),
    gender: Joi.string()
      .max(100)
      .min(3),
    deliveryAddress: Joi.string(),
    userType: Joi.string(),
    active: Joi.boolean(),
    isSuperUser: Joi.boolean(),
    isAdmin: Joi.boolean(),
    isStaff: Joi.boolean(),
  };
  return Joi.validate(user, schema, { abortEarly: false });
}

module.exports = { User, validateUser, validateUserForUpdate };
